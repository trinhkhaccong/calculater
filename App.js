import React from 'react';
import {
  SafeAreaView
} from 'react-native';

import {Provider as PaperProvider} from 'react-native-paper';
import Dashboard from './component/dashboard';

const App = () => {
  return (
        <SafeAreaView style={{backgroundColor: '#FFFFFF', flex: 1}}>
          <Dashboard />
        </SafeAreaView>
  );
};

export default App;
